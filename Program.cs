﻿using System;
using System.IO;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace TimeTrees
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = @"..\..\..\";
            List<TimelineEvent> timeline = new List<TimelineEvent>();
            List<Person> people = new List<Person>();
            bool trueInput = false;

            do
            {
                Console.WriteLine("Введите число 1, если хотите считать данные из CSV файлов, или 2, если из JSON");
                string fileExtension = Console.ReadLine();
                
                if (fileExtension == "1")
                {
                    timeline = ReadTimeLineCsv(path + "timeline.csv");
                    people = ReadPeopleCsv(path + "people.csv");
                    trueInput = true;
                }else if (fileExtension == "2")
                {
                    timeline = ReadTimelineJson(path + "timeline.json");
                    people = ReadPersonJSON(path + "people.json");
                    trueInput = true;
                }
                
            } while (!trueInput);
            

            DateTime first = GetEalierTime(timeline);
            DateTime last = GetLastTime(timeline);

            string timelineDuration = GetDatesDuration(first, last);
            Console.WriteLine(timelineDuration);
            
            List<Person> leapLess20 = GetPeopleInLeap(people);
            foreach (Person person in leapLess20)
            {
                Console.WriteLine(person.Name);
            }
        }

        static List<TimelineEvent> ReadTimeLineCsv(string path)
        {
            string[] lines = File.ReadAllLines(path);
            List<TimelineEvent> timeline = new List<TimelineEvent>();

            foreach (string line in lines)
            {
                string[] temp = line.Split(";");
                TimelineEvent timelineEvent = new TimelineEvent(
                    DateTime.Parse(temp[0]),
                    temp[1]);
                timeline.Add(timelineEvent);
            }
            return timeline;
        }

        static List<TimelineEvent> ReadTimelineJson(string path)
        {
            string data = File.ReadAllText(path);
            List<TimelineEvent> timelineEvents = JsonConvert.DeserializeObject<List<TimelineEvent>>(data);

            return timelineEvents;
        }

        static List<Person> ReadPeopleCsv(string path)
        {
            string[] lines = File.ReadAllLines(path);
            List<Person> people = new List<Person>();

            foreach (string line in lines)
            {
                string[] temp = line.Split(";");
                Person person = new Person(
                    Convert.ToInt32(temp[0]),
                    temp[1],
                    DateTime.Parse(temp[2]),
                    DateTime.Parse(temp[3]));
                people.Add(person);
            }

            return people;
        }

        static List<Person> ReadPersonJSON(string path)
        {
            string data = File.ReadAllText(path);
            List<Person> people = JsonConvert.DeserializeObject<List<Person>>(data);

            return people;
        }

        static DateTime GetEalierTime(List<TimelineEvent> timeline)
        {
            DateTime ealierDate = DateTime.MaxValue;
            
            foreach (var timelineEvent in timeline)
            {
                if (timelineEvent.Date < ealierDate)
                {
                    ealierDate = timelineEvent.Date;
                }
            }
            
            return ealierDate;
        }

        static DateTime GetLastTime(List<TimelineEvent> timeline)
        {
            DateTime lastDate = DateTime.MinValue;
            
            foreach (var timelineEvent in timeline)
            {
                if (timelineEvent.Date > lastDate)
                {
                    lastDate = timelineEvent.Date;
                }
            }
            
            return lastDate;
        }

        static string GetDatesDuration(DateTime first, DateTime last)  
        {                                                             
            double days = 0;
            int month = 0;
            int years = last.Year - first.Year;
            
            if (first.Month > last.Month)
            {
                month = 12 - (first.Month - last.Month);
                years--;
            }
            else
            {
                month = last.Month - first.Month;
            }
            if (first.Day > last.Day)
            {
                days = 30.44 - (first.Day - last.Day);
                month--;
            }
            else
            {
                days = last.Day - first.Day;
            }
            if (month < 0)
            {
                month += 12;
                years--;
            }
            string result = $"Разница между самой первой и последней датой:" +
                            $"{years} лет, {month} месяцев, {(int)days} дней.";
            return result;
        }

        static List<Person> GetPeopleInLeap(List<Person> persons)
        {
            List<Person> result = new List<Person>();

            foreach (Person person in persons)
            {
                int age = GetAge(person);
                if (DateTime.IsLeapYear(person.BirthDate.Year) && age <= 20)
                    result.Add(person);
            }
            return result;
        }

        static int GetAge(Person person)
        {
            int age = person.DeathDate.Year - person.BirthDate.Year;
            if (person.DeathDate.DayOfYear < person.BirthDate.DayOfYear)
                age = age - 1;

            return age;
        }

    }

    struct TimelineEvent
    {
        [JsonProperty("date")]
        public DateTime Date { get; private set; }
        [JsonProperty("eventDescription")]
        public string EventDescription{ get; private set; }

        public TimelineEvent(DateTime date, string description)
        {
            Date = date;
            EventDescription = description;
        }
    }

    struct Person
    {
        [JsonProperty("id")]
        public int Id { get; private set; }
        [JsonProperty("name")]
        public string Name { get; private set; }
        [JsonProperty("birthDate")]    
        public DateTime BirthDate { get; private set; }
        [JsonProperty("deathDate")]
        public DateTime DeathDate { get; private set; }

        public Person(int id, string name, DateTime birthDate, DateTime deathDate)
        {
            Id = id;
            Name = name;
            BirthDate = birthDate;
            DeathDate = deathDate;
        }
    }
}